
	.ORIG	x3000		;Carl Nesbit

	add	r5,r5,#0	;Counter
	add	r4,r4,#-9	;# of guesses
	add	r1,r1,#6	;store 6
	not	r2,r1		;Start turning it negitive
	add	r2,r2,#1	;Finish turning it negitive

	LEA	R0,PROMPT1 	;ask for input
START	TRAP	x20		;get input
	add	r5,r5,#1	;Counter
	add	r6,r5,r4	;Guess number
	BRnz	LOSE	
	add	r0,r2,r0	;find out if they're right
	BRp	BIGG
	BRn	SMMA
	BRz	RIGHT



BIGG	LEA 	R0,BIG		;Too small
	LEA 	R0,PROMPT2	;Guess again
	BR	START

SMMA	LEA 	R0,SMALL	;Really Too Big
	LEA 	R0,PROMPT2	;Guess again
	BR	START

RIGHT	LEA 	R0,RIGHT1
	add	R0,R5,#0
	out
	LEA	R0,RIGHT2
	TRAP 	x25

LOSE	LEA 	R0,LOST		;Game over - lose
	TRAP 	x25		;HALT

PROMPT1	.STRINGZ "Guess a number:"
PROMPT2	.STRINGZ "Guess again:"
SMALL	.STRINGZ "Too small."
BIG 	.STRINGZ "Too big." 
RIGHT1	.STRINGZ "Correct! You took "
RIGHT2	.STRINGZ " guesses." 
ERROR	.STRINGZ "Invalid input."
LOST	.STRINGZ "Game over. Corect answer is 6."

	.END